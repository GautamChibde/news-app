package com.chibde.newsapp.model

data class HeadLine(
    val status: String?,
    val totalResults: Int?,
    val articles: List<Article>
)
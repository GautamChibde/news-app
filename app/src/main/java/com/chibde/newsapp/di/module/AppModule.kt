package com.chibde.newsapp.di.module

import com.chibde.newsapp.api.NewsApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun getRetrofitService(): NewsApiService {
        return NewsApiService.invoke()
    }
}
package com.chibde.newsapp.di.module

import com.chibde.newsapp.ui.HeadLineListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeHeadlineFragment(): HeadLineListFragment
}
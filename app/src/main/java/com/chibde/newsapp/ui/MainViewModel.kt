package com.chibde.newsapp.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import com.chibde.newsapp.api.NewsApiService
import com.chibde.newsapp.model.HeadLine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MainViewModel @Inject constructor(
    val service: NewsApiService
) : ViewModel() {

    fun getHeadLines() {
        service.getTopHeadlines(country = "us", category = "business")
            .enqueue(object : Callback<HeadLine> {
                override fun onFailure(call: Call<HeadLine>, t: Throwable) {

                }

                override fun onResponse(call: Call<HeadLine>, response: Response<HeadLine>) {
                    Log.i("test", "${response.body()}")
                }

            })
    }

}